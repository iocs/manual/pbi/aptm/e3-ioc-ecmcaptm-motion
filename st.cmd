epicsEnvSet IOC PBI-APTM01:Ctrl-ECAT-200

require essioc
require ecmccfg 8.0.0

iocshLoad "${ecmccfg_DIR}/startup.cmd" "ECMC_VER=8.0.2, NAMING=ESSnaming"

iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EK1100
iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EL1808
iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EL5101
iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EL5101
iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EL5101
iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EL5101

# The Phytron VSS 52.200.5 + EL7041-0052 combo is not available in ecmccfg.
# Use addSlave and manually configure it below.
# If this ever gets added to ecmccfg switch back to configureSlave.

iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EL7041-0052

# Set max current to 5A (unit 1mA)
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x1,5000,2)"
# Reduced current 0mA (unit 1mA)
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x2,0,2)"
# Nominal voltage 48V (unit 1mV)
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x3,48000,2)"
# Coil resistance 0.165 Ohm (unit 10mOhm) (rounded to 0.17 Ohm)
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x4,17,2)"
# Motor full steps count 200
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x6,200,2)"

iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EL7041-0052

# Set max current to 5A (unit 1mA)
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x1,5000,2)"
# Reduced current 0mA (unit 1mA)
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x2,0,2)"
# Nominal voltage 48V (unit 1mV)
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x3,48000,2)"
# Coil resistance 0.165 Ohm (unit 10mOhm) (rounded to 0.17 Ohm)
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x4,17,2)"
# Motor full steps count 200
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x6,200,2)"

iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EL7041-0052

# Set max current to 5A (unit 1mA)
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x1,5000,2)"
# Reduced current 0mA (unit 1mA)
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x2,0,2)"
# Nominal voltage 48V (unit 1mV)
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x3,48000,2)"
# Coil resistance 0.165 Ohm (unit 10mOhm) (rounded to 0.17 Ohm)
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x4,17,2)"
# Motor full steps count 200
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x6,200,2)"

iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EL7041-0052

# Set max current to 5A (unit 1mA)
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x1,5000,2)"
# Reduced current 0mA (unit 1mA)
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x2,0,2)"
# Nominal voltage 48V (unit 1mV)
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x3,48000,2)"
# Coil resistance 0.165 Ohm (unit 10mOhm) (rounded to 0.17 Ohm)
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x4,17,2)"
# Motor full steps count 200
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x6,200,2)"

iocshLoad "${ecmccfg_DIR}/configureAxis.cmd" "CONFIG=${E3_CMD_TOP}/cfg/axis1.ax"
iocshLoad "${ecmccfg_DIR}/configureAxis.cmd" "CONFIG=${E3_CMD_TOP}/cfg/axis2.ax"
iocshLoad "${ecmccfg_DIR}/configureAxis.cmd" "CONFIG=${E3_CMD_TOP}/cfg/axis3.ax"
iocshLoad "${ecmccfg_DIR}/configureAxis.cmd" "CONFIG=${E3_CMD_TOP}/cfg/axis4.ax"

iocshLoad "${essioc_DIR}/common_config.iocsh"

ecmcConfigOrDie "Cfg.EcApplyConfig(1)"
iocshLoad "${ecmccfg_DIR}/setAppMode.cmd"
